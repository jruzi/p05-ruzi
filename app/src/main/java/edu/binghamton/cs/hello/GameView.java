package edu.binghamton.cs.hello;

/**
 * Created by pmadden on 2/23/16.
 */

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.ImageView;
import android.content.Context;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static java.util.Random.*;

public class GameView extends View {
    private int mClicks, tapped;
    private float lastX, lastY;
    private Paint mBorderPaint = new Paint();
    private Paint mTextPaint = new Paint();
    private Paint mCirclePaint = new Paint();
    private Timer mTimer;
    private TimerTask mTask;
    private Canvas mCanvas;
    private View self;
    private Boolean circleFinished = true;
    private Random r = new Random();
    private int score = 0;
    //private ImageView apple = new ImageView(getContext());

    public void start()
    {
        tapped = 1;
        mBorderPaint.setColor(0xFFCCBB00);
    }

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mClicks = 0;
        tapped = 0;
        lastY = getHeight() / 2;
        lastX = 0;
        mBorderPaint.setColor(0xFFFF0000);
        mTextPaint.setColor(0xFF101010);
        mCirclePaint.setColor(0xFFF00000);
        self = this;

        //apple.setImageDrawable(Drawable.createFromPath("@drawable/apple1"));

        // mTask = new TimerScheduleFixedRate();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Method
                self.postInvalidate();



                mBorderPaint.setColor(0xFFFF0000);

            }
        }, 0, 20);

        // mTimer.scheduleAtFixedRate();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (action == MotionEvent.ACTION_DOWN) {
            int index = event.getActionIndex();
            //lastX = event.getX(index);
            //lastY = event.getY(index);
            if ((event.getX() > (lastX-50)) && (event.getX() < (lastX+50)) && (event.getY() > (lastY-50)) && (event.getY() < (lastY+50))) {
                score++;
                circleFinished = true;
            }
            this.postInvalidate();
        }
        return true;
    }
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (circleFinished) {
            lastY = r.nextInt(getHeight() - 100);
            if (lastY < 50)
                lastY = 50;
            lastX = 1;
            circleFinished = false;
        } else {
            lastX+= 10;
        }

        if (lastX >= getWidth() + 50) {
            circleFinished = true;
        }

        //canvas.drawRect(0, 0, getWidth(), getHeight(), mBorderPaint);
        String message = "Score:" + Integer.toString(score);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        //mTextPaint.setTextScaleX(15);
        mTextPaint.setTextSize(50);
        canvas.drawText(message, getWidth() / 2, getHeight() / 2, mTextPaint);
        if (lastX > 0) {
            canvas.drawCircle(lastX, lastY, 100, mCirclePaint);
            //apple.setX(lastX);
            //apple.setY(lastY);
            //canvas.drawText("CS441", lastX, lastY, mTextPaint);

        }
    }
}
